﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreChangeController : MonoBehaviour
{
    public GameController gameController;

    public OneScoreChangeController moraleChange;
    public OneScoreChangeController prChange;
    public OneScoreChangeController moneyChange;

    private void Awake()
    {
        Debug.Assert(gameController != null);
        Debug.Assert(moraleChange != null);
        Debug.Assert(prChange != null);
        Debug.Assert(moneyChange != null);
    }

    public void UpdateContents()
    {
        var previous = gameController.PreviousState;
        var current = gameController.CurrentState;
        moraleChange.SetScoreChange(current.getValue("sz") - previous.getValue("sz"));
        prChange.SetScoreChange(current.getValue("pr") - previous.getValue("pr"));
        moneyChange.SetScoreChange(current.getValue("eur") - previous.getValue("eur"));
    }
}
