﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Definition of a project in the game.
/// </summary>
[CreateAssetMenu(fileName = "Project", menuName = "Projects/Definition")]
public class Project : ScriptableObject
{
    /// <summary>
    /// The name of the project.
    /// </summary>
    public string projectName;

    /// <summary>
    /// The name of the (company) state variable that contains the quality of the project.
    /// </summary>
    public string qualityStateName;

    /// <summary>
    /// The name of the (company) state variable that specifies whether the project
    /// was started. The project is started when this variable is greater or equal
    /// to one.
    /// </summary>
    public string startedStateName;

    /// <summary>
    /// The initial state values of the project. These are applied at the beginning
    /// of the game, regardless of when the project is started.
    /// </summary>
    public List<CardEffect> initialEffects = new List<CardEffect>();
}