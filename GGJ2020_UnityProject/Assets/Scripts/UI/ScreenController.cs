﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScreenController : MonoBehaviour
{
    public ApplicationController screenController = null;

    public ApplicationState visibleInState = ApplicationState.UnknownState;

    public UnityEvent onActivated;

    void Awake()
    {
        Debug.Assert(screenController != null);
        screenController.ApplicationStateChanged += OnApplicationStateChanged;
    }

    private void OnApplicationStateChanged(object sender, System.EventArgs e)
    {
        bool isActive = screenController.ApplicationState == visibleInState;
        gameObject.SetActive(isActive);
        if (isActive)
        {
            onActivated?.Invoke();
        }
    }
}
