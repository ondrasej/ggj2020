﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryScreenController : MonoBehaviour
{
    public GameController gameController = null;
    public Text quarterSummary = null;

    private void Awake()
    {
        Debug.Assert(gameController != null);
        Debug.Assert(quarterSummary != null);
    }

    public void UpdateContents()
    {
        quarterSummary.text = gameController.gameState.GetSummary();
    }
}
