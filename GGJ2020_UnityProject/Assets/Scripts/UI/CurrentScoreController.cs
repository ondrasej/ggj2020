﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentScoreController : MonoBehaviour
{
    public Text morale = null;
    public Text pr = null;
    public Text money = null;

    public GameController gameController = null;

    // Start is called before the first frame update
    private void Awake()
    {
        Debug.Assert(morale != null);
        Debug.Assert(pr != null);
        Debug.Assert(money != null);
        Debug.Assert(gameController != null);
    }

    public void UpdateScores()
    {
        var state = gameController.CurrentState;
        morale.text = state.getValue("sz").ToString();
        pr.text = state.getValue("pr").ToString();
        money.text = state.getValue("eur").ToString();
    }
}
