﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionScreenController : MonoBehaviour
{
    public GameController gameController = null;

    public Text eventDescription = null;

    public Button[] choiceButtons;

    private void Awake()
    {
        Debug.Assert(gameController != null);
        Debug.Assert(eventDescription != null);
        Debug.Assert(choiceButtons != null);
        foreach (var button in choiceButtons)
        {
            Debug.Assert(button != null);
        }
    }

    public void UpdateQuestion()
    {
        var gameState = gameController.gameState;
        var card = gameState.GetCurrentCard();
        eventDescription.text = gameState.FormatString(card.cardDescription);
        for (int i = 0; i < card.options.Length; i++)
        {
            choiceButtons[i].gameObject.SetActive(true);
            var text = choiceButtons[i].GetComponentInChildren<Text>();
            text.text = gameState.FormatString(card.options[i].name);
        }
        for (int i = card.options.Length; i < choiceButtons.Length; i++)
        {
            choiceButtons[i].gameObject.SetActive(false);
        }
    }

    public void OnPlayerChoice(int option)
    {
        gameController.MakePlayerChoice(option);
    }
}
