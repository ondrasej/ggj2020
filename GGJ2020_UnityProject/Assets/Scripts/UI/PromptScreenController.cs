﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptScreenController : MonoBehaviour
{
    public GameController gameController = null;

    public Text playerNameText = null;

    public Text descriptionText = null;

    public Button showGoalsButton = null;

    public string defaultText = "";

    private void Awake()
    {
        Debug.Assert(gameController != null);
        Debug.Assert(playerNameText != null);
        Debug.Assert(descriptionText != null);
        Debug.Assert(showGoalsButton != null);
    }

    public void UpdateContents()
    {
        playerNameText.text = string.Format("CEO {0}", gameController.gameState.CurrentPlayer + 1);
        descriptionText.text = defaultText;
        showGoalsButton.gameObject.SetActive(true);
    }

    public void ShowPlayerGoals()
    {
        descriptionText.text = gameController.gameState.CurrentPlayerGoals;
        showGoalsButton.gameObject.SetActive(false);
    }
}
