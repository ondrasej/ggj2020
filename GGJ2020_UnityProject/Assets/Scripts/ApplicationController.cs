﻿using UnityEngine;

public enum ApplicationState
{
    UnknownState,
    TitleScreen,
    PlayerPrompt,
    Question,
    Summary,
    VotingPrompt,
    PlayerVoting,
    EndGame,
}

public class ApplicationController : MonoBehaviour
{
    private ApplicationState _applicationState = ApplicationState.UnknownState;
    public ApplicationState ApplicationState
    {
        get => _applicationState; set
        {
            if (value != _applicationState)
            {
                System.EventHandler handler = ApplicationStateChanged;
                _applicationState = value;
                handler?.Invoke(this, new System.EventArgs());
            }
        }
    }
    public event System.EventHandler ApplicationStateChanged;

    public GameController gameController;

    private void Awake()
    {
        Debug.Assert(gameController != null);

        // Make sure that all screen objects are active before the start of the game.
        foreach (var screen in Resources.FindObjectsOfTypeAll<ScreenController>())
        {
            screen.gameObject.SetActive(true);
        }
    }

    private void Start()
    {
        ShowTitleScreen();
    }

    public void ShowTitleScreen()
    {
        ApplicationState = ApplicationState.TitleScreen;
    }

    public void StartNewGame()
    {
        Debug.Assert(ApplicationState == ApplicationState.TitleScreen);
        gameController.StartNewGame();
        ApplicationState = ApplicationState.PlayerPrompt;
    }

    public void OnMoveToQuestion()
    {
        Debug.Assert(ApplicationState == ApplicationState.PlayerPrompt);
        ApplicationState = ApplicationState.Question;
    }
}

