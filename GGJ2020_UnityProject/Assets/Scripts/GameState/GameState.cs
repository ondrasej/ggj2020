﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// The exception raised when the game state does not have enough events to deal in the next round.
/// </summary>
public class OutOfCards : System.Exception
{
    public readonly List<CardData> cards;

    public OutOfCards(List<CardData> cards)
        : base(string.Format("We're out of cards, available: {0}",
                             cards.Select(card => card.cardName).JoinStrings(", ")))
    {
        this.cards = cards;
    }
}

[System.Serializable]
public class GameState
{
    public enum NextStep
    {
        NextPlayer,
        EndOfQuarter,
        EndOfGame
    }

    public delegate void PlayerChanged(int index, PlayerType type);
    public delegate void PlayerChoince(int playerIndex, int choiceIndex, CardEffect[] effects);
    public event PlayerChanged OnPlayerChanged;
    public event PlayerChoince OnPlayerMakeChoince;

    /// <summary>
    /// The number of players in the game.
    /// </summary>
    public static readonly int numPlayers = 3;

    /// <summary>
    /// The number of quarters the game takes.
    /// </summary>
    public static readonly int numQuarters = 6;

    public int currentQuarter;

    public int evilPlayer;

    private int currentPlayer;

    public List<CardData> currentQuarterCards = new List<CardData>();

    public List<CardData> availableCards;

    public CardRepository cardRepository = null;

    public ConditionalEffectRepository conditionalEffectRepository = null;

    public List<Project> projectDefinitions = null;

    public List<ConditionalEffect> availableConditionalEffects;

    public State companyState;

    public List<CardEffect>[] delayedEffects;

    public PlayerType[] playerTypes;

    public string[] playerNames = new string[] { "CEO 1", "CEO 2", "CEO 3" };

#if !NO_LOG
    private StringBuilder logBuilder = new StringBuilder();
    public List<List<CardData>> cardLog = new List<List<CardData>>();
    public List<int[]> choiceLog = new List<int[]>();
#endif

    // Mapping for played cards: name -> player ID.
    public Dictionary<string, int> cardToPlayer = new Dictionary<string, int>();

    public int CurrentPlayer
    {
        get => currentPlayer; set
        {
            currentPlayer = value;
            if (OnPlayerChanged != null)
            {
                OnPlayerChanged.Invoke(value, playerTypes[value]);
            }

        }
    }

    public string CurrentPlayerGoals
    {
        get => PlayerTypeData.Goals[playerTypes[currentPlayer]];
    }

    public GameState(CardRepository cardRepository,
                     ConditionalEffectRepository conditionalEffectRepository,
                     ProjectRepository projectRepository,
                     ICollection<CardEffect> initialCompanyEffects)
    {
        this.cardRepository = cardRepository;
        this.conditionalEffectRepository = conditionalEffectRepository;
        this.projectDefinitions = projectRepository.Projects.ToList();

        delayedEffects = new List<CardEffect>[numQuarters];
        for (int i = 0; i < delayedEffects.Length; i++)
            delayedEffects[i] = new List<CardEffect>();

        playerTypes = new PlayerType[numPlayers];
        var playerTypeList = System.Enum.GetValues(typeof(PlayerType));
        for (int i = 0; i < numPlayers; ++i)
        {
            // First assign non-evil roles to everyone.
            var playerTypeIndex = Random.Range(1, playerTypeList.Length);
            playerTypes[i] = (PlayerType)playerTypeList.GetValue(playerTypeIndex);
        }
        // Then pick one as the evil player.
        evilPlayer = Random.Range(0, numPlayers);
        playerTypes[evilPlayer] = PlayerType.Evil;

        availableCards = cardRepository.Cards.ToList();
        availableConditionalEffects = conditionalEffectRepository.effects.ToList();

        companyState = new State(initialCompanyEffects);
        foreach(var project in projectDefinitions)
        {
            foreach (var effect in project.initialEffects)
            {
                if (effect.delay == 0)
                {
                    companyState.addEffect(effect.statName, effect.value);
                }
                else
                {
                    delayedEffects[effect.delay].Add(effect);
                }
            }
        }

#if !NO_LOG
        logBuilder.AppendLine("-- start --");
        logBuilder.AppendFormat("Player types: {0}\n", playerTypes.Select(x => x.ToString()).JoinStrings(", "));
        logBuilder.AppendLine("Initial state:");
        companyState.AddToLog(logBuilder, "  ");
#endif

        currentQuarter = -1;
        currentPlayer = -1;
        MoveToNextQuarter();
    }

    public string GetLog()
    {
        return logBuilder.ToString();
    }

    public CardData GetCurrentCard()
    {
        return currentQuarterCards[CurrentPlayer];
    }

    public string StringSubstitution(string template)
    {
        var builder = new System.Text.StringBuilder();
        int startPos = 0;
        while (startPos < template.Length)
        {
            int nextPos = template.IndexOf('{', startPos);
            if (nextPos == -1)
            {
                builder.Append(template.Substring(startPos, template.Length - startPos));
                return builder.ToString();
            }
            builder.Append(template.Substring(startPos, nextPos - startPos));
            startPos = nextPos + 1;
            nextPos = template.IndexOf('}', startPos);
            string variable = template.Substring(startPos, nextPos - startPos);
        }
        return builder.ToString();
    }

    public void MoveToNextQuarter()
    {
        ++currentQuarter;
        CurrentPlayer = 0;

        // Keep the current quarter in company state, so that we can have conditions
        // based on it.
        companyState.addEffect("quarter", 1);

#if !NO_LOG
        logBuilder.AppendFormat("-- quarter {0} --\n", currentQuarter + 1);
#endif

        if (currentQuarter <= 5)
        {
            currentQuarterCards = DrawCardsForPlayers();
            Debug.Assert(currentQuarterCards.Count == numPlayers);
#if !NO_LOG
            cardLog.Add(currentQuarterCards);
            choiceLog.Add(new int[numPlayers]);
#endif
        }
    }


    public NextStep MakePlayerChoice(int option)
    {
        var card = currentQuarterCards[CurrentPlayer];
        var chosenOption = card.options[option];

#if !NO_LOG
        logBuilder.AppendFormat(
            "Player {0}, card {1}, option {2}\n",
            CurrentPlayer, card.cardName, option);
        choiceLog[currentQuarter][currentPlayer] = option;
#endif

        foreach (var effect in chosenOption.companyEffects)
        {
            //Debug.LogFormat("Adding company effect: {0}: {1}", effect.statName, effect.value);
            int targetQuarter = currentQuarter + effect.delay;
            if (targetQuarter < numQuarters)
            {
                delayedEffects[currentQuarter + effect.delay].Add(effect);
            }
            else
            {
                // Ignore delayed effects that would happen after the last quarter
                // of the game, but complain to the log.
                Debug.LogFormat("Delayed effect will appear after the last quarter. Card: {0}", card.cardName);
            }
        }

        if (OnPlayerMakeChoince != null)
        {
            OnPlayerMakeChoince.Invoke(CurrentPlayer, option, chosenOption.companyEffects);
        }

        if (CurrentPlayer + 1 == numPlayers)
        {
            return currentQuarter + 1 == numQuarters ?
                   NextStep.EndOfGame : NextStep.EndOfQuarter;
        }
        else
        {
            currentPlayer++;
            return NextStep.NextPlayer;
        }
    }

    public void ProcessEndOfQuarter()
    {
        // Apply effects.
        foreach (var effect in delayedEffects[currentQuarter])
        {
            companyState.addEffect(effect.statName, effect.value);
        }

        // Apply conditional effects.
        var appliedOneTimeEffects = new HashSet<ConditionalEffect>();
        foreach(var conditionalEffect in availableConditionalEffects)
        {
            if (StateUtil.AreRequirementsValid(conditionalEffect.requirements, companyState))
            {
#if !NO_LOG
                logBuilder.AppendFormat("Conditional effect: {0}\n", conditionalEffect.name);
#endif
                // TODO(ondrasej): Make a difference between immediate and delayed effects.
                foreach (var effect in conditionalEffect.effects)
                {
                    companyState.addEffect(effect.statName, effect.value);
                }
                if (conditionalEffect.effectType == ConditionalEffectType.SINGLE_USE)
                {
                    appliedOneTimeEffects.Add(conditionalEffect);
                }
            }
        }
        availableConditionalEffects.RemoveAll(effect => appliedOneTimeEffects.Contains(effect));

#if !NO_LOG
        logBuilder.AppendLine("End of quarter state:");
        companyState.AddToLog(logBuilder, "  ");
#endif
    }

    public List<CardData> DrawCardsForPlayers()
    {
        var players = RandomPlayerOrder();
        var cards = availableCards.Where(card => StateUtil.isCardValid(card, companyState)).ToList();
#if !NO_LOG
        logBuilder.AppendFormat(
            "Available: {0}\n",
            cards.Select(card => card.cardName).OrderBy(x => x).JoinStrings(", "));
#endif

        if (cards.Count < numPlayers) throw new OutOfCards(cards);
        cards = WeightedRandomSort(cards);

        var cardIndices = new int[numPlayers];
        for (int i = 0; i < cardIndices.Length; ++i) cardIndices[i] = -1;

        System.Func<int, int, bool> UsedByOtherPlayer = (card, player) =>
        {
            // We count the number of times the card is currently selected; one selection
            // is for the current player, we do not care about that one.
            for (int i = 0; i < numPlayers; ++i)
            {
                if (i != player && cardIndices[i] == card) return true;
            }
            return false;
        };

        for (int playerIndex = 0;
             playerIndex < numPlayers && cardIndices[players[0]] < cards.Count;)
        {
            var player = players[playerIndex];
            var isEvil = player == evilPlayer;
            var removedUserType = isEvil ? CardUserType.GOOD : CardUserType.EVIL;

            while (true)
            {
                var cardIndex = ++cardIndices[player];
                if (cardIndex >= cards.Count) break;
                if (UsedByOtherPlayer(cardIndex, player)) continue;
                var card = cards[cardIndex];
                if (card.userType == removedUserType) continue;
                int prevPlayer = -1;
                if (!string.IsNullOrEmpty(card.samePlayerAsCard) &&
                    cardToPlayer.TryGetValue(card.samePlayerAsCard, out prevPlayer))
                {
                    if (prevPlayer != player) continue;
                }
                break;
            }

            // If we found a card, move to the next player. Otherwise, backtrack.
            if (cardIndices[player] < cards.Count)
            {
                playerIndex++;
                // Reset card selection for subsequent players.
                for (int i = playerIndex; i < numPlayers; ++i)
                {
                    cardIndices[players[playerIndex]] = -1;
                }
            }
            else
            {
                playerIndex--;
            }
        }

        if (cardIndices[players[0]] >= cards.Count)
        {
            // Backtracking failed, raise an exception.
            throw new OutOfCards(cards);
        }

        var res = cardIndices.Select(i => cards[i]).ToList();
        var numRemovedCards = availableCards.RemoveAll(card => res.Contains(card));
        Debug.Assert(numRemovedCards == numPlayers);

#if !NO_LOG
        logBuilder.AppendFormat(
            "Chosen: {0}\n",
            res.Select(card => card.cardName).OrderBy(x => x).JoinStrings(", "));
#endif

        for (int i = 0; i < res.Count; i++)
        {
            cardToPlayer[res[i].cardName] = i;
        }

        return res;
    }

    private static CardData WeightedChoice(List<CardData> cards)
    {
        Debug.Assert(cards.Count > 0, "No cards to choose from!");
        var sum = cards.Select(card => card.weight).Sum();
        var selector = Random.Range(0.0f, sum);
        var partialSum = 0.0f;
        foreach (var card in cards)
        {
            partialSum += card.weight;
            if (selector <= partialSum) return card;
        }
        Debug.LogError("Selector did not pick any card!");
        return null;
    }

    public static List<CardData> WeightedRandomSort(List<CardData> cards)
    {
        Debug.Assert(cards.Count > 0, "No cards to sort.");
        // Move all follow-up cards to the front.
        int firstNonFollowUpCard = 0;
        for (int selectedCard = 0; selectedCard < cards.Count; ++selectedCard)
        {
            if (!string.IsNullOrEmpty(cards[selectedCard].samePlayerAsCard))
            {
                var tmp = cards[firstNonFollowUpCard];
                cards[firstNonFollowUpCard] = cards[selectedCard];
                cards[selectedCard] = tmp;
                ++firstNonFollowUpCard;
            }
        }

        // Do a weighted sort of the remaining cards.
        var sum = cards.Where(card => string.IsNullOrEmpty(card.samePlayerAsCard))
                       .Select(card => card.weight)
                       .Sum();
        for (int selectedCard = firstNonFollowUpCard;
             selectedCard < cards.Count - 1;
             ++selectedCard)
        {
            var selector = Random.Range(0.0f, sum);
            var partialSum = 0.0f;
            for (int i = selectedCard; i < cards.Count; ++i)
            {
                partialSum += cards[i].weight;
                if (selector <= partialSum)
                {
                    var tmp = cards[selectedCard];
                    cards[selectedCard] = cards[i];
                    cards[i] = tmp;
                    sum -= cards[selectedCard].weight;
                    break;
                }
            }
        }

        // OrderBy does a random sort, so the randomized order is preserved for each priority.
        return cards.OrderBy(card => card.priority).ToList();
    }

    public static int[] RandomPlayerOrder()
    {
        var res = new int[numPlayers];
        // Fill in the array.
        for (int i = 0; i < numPlayers; ++i) res[i] = i;
        // Shuffle it randomly.
        for (int i = 0; i < numPlayers - 1; ++i)
        {
            var j = Random.Range(i, res.Length);
            var tmp = res[i];
            res[i] = res[j];
            res[j] = tmp;
        }
        return res;
    }

    private static List<T> PickRandom<T>(List<T> list, int numItems)
    {
        var res = new List<T>();
        var indices = new int[list.Count];
        for (int i = 0; i < indices.Length; ++i) indices[i] = i;

        var remaining = indices.Length;
        for (int i = 0; i < numItems; ++i)
        {
            var selected = Random.Range(0, remaining);
            res.Add(list[indices[selected]]);
            indices[selected] = indices[remaining - 1];
            --remaining;
        }   
        return res;
    }

    public string FormatString(string template)
    {
        var res = new System.Text.StringBuilder();
        int textStartPos = 0;
        int varStartPos = template.IndexOf('{');
        while (varStartPos != -1)
        {
            res.Append(template, textStartPos, varStartPos - textStartPos);
            var varEndPos = template.IndexOf('}', varStartPos);
            Debug.Assert(varEndPos != -1);
            var varName = template.Substring(varStartPos + 1, varEndPos - varStartPos - 1);
            if (varName == "quarter")
            {
                // We need to convert from 0-based to 1-based index.
                res.Append(currentQuarter + 1);
            }
            else if (varName == "evil-player")
            {
                res.Append(playerNames[evilPlayer]);
            }
            else if (varName.StartsWith("player-"))
            {
                int player = int.Parse(varName.Substring(7));
                Debug.Assert(player >= 0);
                Debug.Assert(player < numPlayers);
                res.Append(playerNames[player]);
            }
            else if (companyState.Contains(varName))
            {
                res.Append(companyState.getValue(varName));
            }
            else
            {
                Debug.LogErrorFormat("Invalid variable name: {0}", varName);
            }
            textStartPos = varEndPos + 1;
            varStartPos = template.IndexOf('{', varEndPos);
        }
        varStartPos = template.Length;
        res.Append(template, textStartPos, varStartPos - textStartPos);
        return res.ToString();
    }

    public string GetSummary()
    {
        var builder = new System.Text.StringBuilder();
        foreach (var effect in delayedEffects[currentQuarter])
        {
            if (builder.Length > 0) builder.AppendLine();
            if (effect.newsItems == null || effect.newsItems.Count == 0) continue;
            var newsItem = effect.newsItems[Random.Range(0, effect.newsItems.Count)];
            builder.AppendLine(FormatString(newsItem));
        }
        if (builder.Length == 0)
            builder.Append("Nic zajímavého se tento kvartál nestalo.");
        return builder.ToString();
    }

    public string GetChoiceSummary()
    {
        StringBuilder builder = new StringBuilder();

        Debug.Assert(choiceLog.Count == cardLog.Count);
        for (int quarter = 0; quarter < choiceLog.Count; ++quarter)
        {
            if (builder.Length > 0) builder.Append(", ");
            builder.Append(cardLog[quarter]
                .Zip(choiceLog[quarter], (card, choice) => string.Format("{0}: {1}", card.cardName, choice))
                .OrderBy(x => x)
                .JoinStrings(", "));
        }
        return builder.ToString();
    }

    public string GetFinalText()
    {
        var builder = new System.Text.StringBuilder();
        if (companyState.getValue("chicken-hired") > 0)
        {
            builder.AppendLine(
                "Untitled Chicken Game naprosto předčila jakákoliv " +
                "očekávání.Dle celého světa se jedná o nejzásadnější " +
                "objev lidstva a vaše šance na výhru v nadcházejících " +
                "prezidentnských volbách jsou podle prognóz celých 100 %" +
                ".Dalajláma by se s vámi rád setkal. Toto je vrchol vašeho života.");
        }
        else if (companyState.getValue("started-chicken") > 0)
        {
            builder.AppendLine(
                "I přes vaše enormní snažení s vlastními projekty celým " +
                "světem otřásla hra s názvem \"Untitled Chicken Game\" " +
                "Tato revoluční hra jako první videohra v historii získala " +
                "nobelovu cenu za zábavu a řada expertů ji označila za " +
                "nejzásadnější objev lidstva.");
        }
        if (companyState.getValue("evil-found") == 0 &&
            companyState.getValue("china-public-sale") > 0)
        {
            builder.Append(
                "Toto je konec. Vaši firmu získala čínská vláda, " +
                "která skrze ni začala trénovat své vojáky. Ještě o " +
                "tom nevíte, ale toto je blížící se konec světa tak, " +
                "jak ho znáte. Kdybyste bývali odhalili zrádce ve " +
                "vlastních řadách, nemuselo to dojít tak daleko. Teď " +
                "se svou chybou musíte žít.");
            return builder.ToString();
        }
        if (companyState.getValue("evil-found") == 0 &&
            companyState.getValue("china-public-sale") == 0)
        {
            builder.Append(
                "Bohužel ani přes veškeré snažení jste neodhalili zrádce " +
                "ve vašich řadách. Vaše firma utrpěla finální ránu v podobě " +
                "otřesného mediálního obrazu a pro zachování důstojnosti " +
                "jste se rozhodli společnost prodat vydavateli Pasivision. " +
                "Ten ji určitě zase postaví na nohy. Nebo taky ne.");
            return builder.ToString();
        }

        return builder.ToString();
    }
}