﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

/// <summary>
/// An asset that contains the list of all projects used in the game,
/// to make testing easier.
/// </summary>
[CreateAssetMenu(fileName = "ProjectRepository", menuName = "Projects/Repository")]
public class ProjectRepository : ScriptableObject
{
    /// <summary>
    /// The list of projects in the repository.
    /// </summary>
    public List<Project> Projects;
}

#if UNITY_EDITOR
/// <summary>
/// A custom editor for the project repository. Adds a button for loading all
/// project definitions in the asset database.
/// </summary>
[CustomEditor(typeof(ProjectRepository))]
public class ProjectRepositoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Load all projects"))
        {
            var repository = target as ProjectRepository;
            LoadAllProjects(repository);
        }
    }

    void LoadAllProjects(ProjectRepository repository)
    {
        repository.Projects = AssetDatabase.FindAssets("t:Project")
            .Select(AssetDatabase.GUIDToAssetPath)
            .OrderBy(x => x)
            .Select(AssetDatabase.LoadAssetAtPath<Project>)
            .ToList();
        EditorUtility.SetDirty(repository);
    }
}
#endif  // UNITY_EDITOR