﻿using System;
using System.Text;
using System.Collections.Generic;

public struct Statistics
{
    public int min;
    public int max;
    public double average;

    public override string ToString()
    {
        return string.Format("min = {0}, max = {1}, avg = {2}", min, max, average);
    }
}

public static class Utils
{
    public static string JoinStrings(this IEnumerable<string> strings, string separator)
    {
        StringBuilder builder = new StringBuilder();
        foreach (var str in strings)
        {
            if (builder.Length > 0) builder.Append(separator);
            builder.Append(str);
        }
        return builder.ToString();
    }

    public static Statistics Statistics(this IEnumerable<int> values)
    {
        var enumerator = values.GetEnumerator();
        var min = int.MaxValue;
        var max = int.MinValue;
        int sum = 0;
        int count = 0;
        while (enumerator.MoveNext())
        {
            var current = enumerator.Current;
            min = current < min ? current : min;
            max = current > max ? current : max;
            sum += current;
            count++;
        }
        var average = count > 0 ? (double)sum / count : 0.0;
        return new Statistics() { min = min, max = max, average = average };
    }

    public static V GetOrDefault<K, V>(this Dictionary<K, V> dict, K key, V defaultValue)
    {
        V value;
        return dict.TryGetValue(key, out value) ? value : defaultValue;
    }
}