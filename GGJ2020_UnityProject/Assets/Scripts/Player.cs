﻿using System.Collections.Generic;

public enum PlayerType
{
    Evil = 0,
    MakingMoney = 1,
    BetacriticFreak = 2,
    SerialPublisher = 3,
    FamilyBusiness = 4,
}

public static class PlayerTypeData
{
    public static readonly IReadOnlyDictionary<PlayerType, string> Goals = new Dictionary<PlayerType, string>()
    {
        {
            PlayerType.Evil,
            "Jsi špeh!\n" +
            "Tvým úkolem je:\n" +
            "Prodat firmu Číně\n" +
            "nebo snížit PR firmy na konci hry pod hodnotu 2."
        },
        {
            PlayerType.BetacriticFreak,
            "Jsi čestný CEO!\n" +
            "Tvým úkolem je:\n" +
            "Získat hodnocení na všech vydaných hrách alespoň 7/10\n" +
            "Udržet firmu při životě.\n"
        },
        {
            PlayerType.FamilyBusiness,
            "Jsi čestný CEO!\n" +
            "Tvým úkolem je:\n" +
            "Udržet spokojenost zaměstnanců nad hodnotou nula po celou dobu hry.\n"+
            "Udržet firmu při životě.\n"
        },
        {
            PlayerType.MakingMoney,
            "Jsi čestný CEO!\n" +
            "Tvým úkolem je:\n" +
            "Mít na konci hry 15 a více peněz.\n"+
            "Udržet firmu při životě.\n"
        },
        {
            PlayerType.SerialPublisher,
            "Jsi čestný CEO!\n" +
            "Tvým úkolem je:\n " +
            "Vydat alespoň dvě hry.\n"+
            "Udržet firmu při životě.\n"
        },
    };
}