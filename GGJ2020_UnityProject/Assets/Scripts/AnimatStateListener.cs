﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatStateListener : StateMachineBehaviour
{
    public event Action<Animator, AnimatorStateInfo> OnEnter;
    public event Action<Animator, AnimatorStateInfo> OnExit;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(OnEnter !=null)
            OnEnter.Invoke(animator, stateInfo);

    }


    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       if(OnExit !=null)
            OnExit.Invoke(animator, stateInfo);
    }


}
