 using UnityEngine;

public enum CardUserType
{
    BOTH,
    EVIL,
    GOOD,
}

[CreateAssetMenu(fileName = "Card", menuName = "Cards/Card", order = 0)]
public class CardData : ScriptableObject
{
    public string cardName;
    
    public string cardDescription;

    public CardRequirement[] requirements;

    public CardOption[] options;
    
    public CardUserType userType;

    public float weight = 1.0f;

    public int priority = 0;
    
    public string samePlayerAsCard;
}
