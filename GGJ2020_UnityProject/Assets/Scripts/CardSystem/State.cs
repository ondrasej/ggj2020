using System.Collections.Generic;
using System.Linq;
using System.Text;

public class State
{
    Dictionary<string, int> values = new Dictionary<string, int>();

    public State()
    {
    }

    public State(ICollection<CardEffect> initialEffects)
    {
        foreach (var effect in initialEffects)
        {
            addEffect(effect.statName, effect.value);
        }
    }

    public ICollection<string> IncludedStats{get {return values.Keys;} }

    public void addEffect(string statistic, int value)
    {
        if (values.ContainsKey(statistic))
        {
            values[statistic] = values[statistic] + value;
        }
        else
        {
            values[statistic] = value;
        }
    }

    public int getValue(string statistic)
    {
        if (values.ContainsKey(statistic))
            return values[statistic];

        return 0;
    }

    public bool Contains(string statName)
    {
        return values.ContainsKey(statName);
    }

    public void AddToLog(StringBuilder log, string prefix = "")
    {
        foreach(var state in IncludedStats.OrderBy(x => x))
        {
            log.AppendFormat("{2}{0}: {1}\n", state, getValue(state), prefix);
        }
    }

    public State Clone()
    {
        return new State() { values = new Dictionary<string, int>(values) };
    }
}


