using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardEffect
{
    public string statName;
    public int value;
    public int delay;

    public List<string> newsItems;
}
