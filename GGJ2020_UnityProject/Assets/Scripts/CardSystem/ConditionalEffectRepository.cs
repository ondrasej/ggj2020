﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "ConditionalEffectRepository", menuName = "Conditional effects/Repository")]
public class ConditionalEffectRepository : ScriptableObject
{
    public List<ConditionalEffect> effects;
}

#if UNITY_EDITOR
[CustomEditor(typeof(ConditionalEffectRepository))]
public class ConditionalEffectRepositoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Load all conditional effects"))
        {
            var repository = target as ConditionalEffectRepository;
            LoadAllConditionalEffects(repository);
        }
    }

    private void LoadAllConditionalEffects(ConditionalEffectRepository repository)
    {
        repository.effects = AssetDatabase.FindAssets("t:ConditionalEffect")
            .Select(AssetDatabase.GUIDToAssetPath)
            .OrderBy(x => x)
            .Select(AssetDatabase.LoadAssetAtPath<ConditionalEffect>)
            .ToList();
        EditorUtility.SetDirty(repository);
    }
}
#endif  // UNITY_EDITOR