﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EffectCollection", menuName = "Effect collection")]
public class EffectCollection : ScriptableObject
{
    public List<CardEffect> effects = new List<CardEffect>();
}