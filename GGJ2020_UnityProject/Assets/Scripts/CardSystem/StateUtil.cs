using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateUtil
{
    public static void MergeTo(State toState, params State[] states)
    {
        foreach (var state in states)
        {
            foreach (string key in state.IncludedStats)
            {
                toState.addEffect(key, state.getValue(key));
            }
        }
    }

    public static bool isCardValid(CardData card, State companyState)
    {
        Debug.Assert(companyState != null);
        return AreRequirementsValid(card.requirements, companyState);
    }

    public static bool AreRequirementsValid(IEnumerable<CardRequirement> requirements, State companyState)
    {
        return requirements.All(requirement => isValueValidByRule(requirement.statName,
                                                                  requirement.value,
                                                                  requirement.rule,
                                                                  companyState));
    }

    public static bool isValueValidByRule(string name, int value, RuleType rule, State byState)
    {
        switch (rule)
        {
            case RuleType.Equal:
                return value == byState.getValue(name);
            case RuleType.Greater:
                return byState.getValue(name) > value;
            case RuleType.Smaller:
                return byState.getValue(name) < value;
            case RuleType.GreaterOrEqual:
                return byState.getValue(name) >= value;
            case RuleType.SmallerOrEqual:
                return byState.getValue(name) <= value;
        }

        return false;
    }
}


