﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

/// <summary>
/// The type of the conditional effect.
/// </summary>
public enum ConditionalEffectType
{
    /// <summary>
    /// The effect is used only in the first quarter when the requirements are met.
    /// </summary>
    SINGLE_USE,

    /// <summary>
    /// The effect is used in all quarters when the requirements are met.
    /// </summary>
    PERMANENT,    
}

/// <summary>
/// A conditional effect is a card effect without an actual card. The effect has
/// a collection of requirements, and when these requirements hold, the effect
/// modifies the game state.
/// </summary>
[CreateAssetMenu(fileName = "ConditionalEffect", menuName = "Conditional effects/Effect")]
public class ConditionalEffect : ScriptableObject
{
    public string effectName;

    public ConditionalEffectType effectType;

    public CardRequirement[] requirements;

    public CardEffect[] effects;
}
