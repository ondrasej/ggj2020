using System;

[Serializable]
public class CardRequirement
{
    public string statName;
    public RuleType rule;
    public int value;
}
