﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "CardRepository", menuName = "Cards/Pack", order = 10)]
public class CardRepository : ScriptableObject
{
    public List<CardData> Cards = new List<CardData>();
}

#if UNITY_EDITOR
[CustomEditor(typeof(CardRepository))]
public class CardRepositoryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Load all cards"))
        {
            var repository = target as CardRepository;
            LoadAllCards(repository);
        }
    }

    void LoadAllCards(CardRepository repository)
    {
        repository.Cards = AssetDatabase.FindAssets("t:CardData")
            .Select(AssetDatabase.GUIDToAssetPath)
            .OrderBy(x => x)
            .Select(AssetDatabase.LoadAssetAtPath<CardData>)
            .ToList();
        EditorUtility.SetDirty(repository);
    }
}
#endif  // UNITY_EDITOR