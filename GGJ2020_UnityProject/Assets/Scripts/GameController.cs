﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public ApplicationController applicationController;

    public CardRepository cardRepository = null;

    public ConditionalEffectRepository conditionalEffectRepository = null;

    public EffectCollection initialEffects = null;

    public ProjectRepository projectRepository = null;

    public GameState gameState;

    public State CurrentState { get => gameState.companyState; }
    public State PreviousState { get; private set; }

    void Awake()
    {
        Debug.Assert(applicationController != null);
        Debug.Assert(cardRepository != null);
        Debug.Assert(conditionalEffectRepository != null);
        Debug.Assert(projectRepository != null);
        Debug.Assert(initialEffects != null);
    }

    public void StartNewGame()
    {
        gameState = new GameState(cardRepository,
                                  conditionalEffectRepository,
                                  projectRepository,
                                  initialEffects.effects);
    }

    public void MakePlayerChoice(int option)
    {
        GameState.NextStep nextStep = gameState.MakePlayerChoice(option);
        switch(nextStep)
        {
            case GameState.NextStep.EndOfGame:
                applicationController.ApplicationState = ApplicationState.EndGame;
                break;
            case GameState.NextStep.EndOfQuarter:
                PreviousState = CurrentState.Clone();
                gameState.ProcessEndOfQuarter();
                applicationController.ApplicationState = ApplicationState.Summary;
                break;
            case GameState.NextStep.NextPlayer:
                applicationController.ApplicationState = ApplicationState.PlayerPrompt;
                break;
        }
    }

    public void MoveToNextQuarter()
    {
        try
        {
            gameState.MoveToNextQuarter();
            applicationController.ApplicationState = ApplicationState.PlayerPrompt;
        }
        catch(OutOfCards)
        {
            applicationController.ApplicationState = ApplicationState.EndGame;
        }
    }
}
