﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OneScoreChangeController : MonoBehaviour
{
    public Text differenceText;
    public GameObject arrowUp;
    public GameObject arrowDown;

    public void Awake()
    {
        Debug.Assert(differenceText != null);
        Debug.Assert(arrowUp != null);
        Debug.Assert(arrowDown != null);
    }

    public void SetScoreChange(int change)
    {
        differenceText.text = string.Format(change > 0 ? "+{0}" : "{0}", change);
        arrowUp.SetActive(change > 0);
        arrowDown.SetActive(change < 0);
    }
}
