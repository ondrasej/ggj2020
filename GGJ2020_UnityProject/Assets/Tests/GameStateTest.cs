﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;

namespace Tests
{
    public class GameStateTests
    {
        public static readonly string CardRepositoryPath = "Assets/Content/CardRepository.asset";
        public static readonly string ConditionalEffectRepositoryPath = "Assets/Content/ConditionalEffectRepository.asset";
        public static readonly string InitialEffectsPath = "Assets/Content/InitialState.asset";
        public static readonly string ProjectRepositoryPath = "Assets/Content/ProjectRepository.asset";

        protected CardRepository cardRepository;
        protected ConditionalEffectRepository conditionalEffectRepository;
        protected EffectCollection initialEffects;
        protected ProjectRepository projectRepository;

        [SetUp]
        public void LoadCards()
        {
            cardRepository = AssetDatabase.LoadAssetAtPath<CardRepository>(CardRepositoryPath);
            conditionalEffectRepository = AssetDatabase.LoadAssetAtPath<ConditionalEffectRepository>(ConditionalEffectRepositoryPath);
            initialEffects = AssetDatabase.LoadAssetAtPath<EffectCollection>(InitialEffectsPath);
            projectRepository = AssetDatabase.LoadAssetAtPath<ProjectRepository>(ProjectRepositoryPath);
        }

        [Test]
        public void TestAllLoaded()
        {
            Assert.NotNull(cardRepository);
            Assert.NotNull(conditionalEffectRepository);
            Assert.NotNull(initialEffects);
            Assert.NotNull(projectRepository);
        }

        [Test]
        public void TestFormatString()
        {
            var state = new GameState(cardRepository,
                                      conditionalEffectRepository,
                                      projectRepository,
                                      initialEffects.effects);
            state.companyState.addEffect("foo-bar", 5);
            state.companyState.addEffect("bar-foo", 0);
            Assert.AreEqual(state.FormatString("Hello {foo-bar}{bar-foo}!"), "Hello 50!");

            state.currentQuarter = 3;
            Assert.AreEqual(state.FormatString("We have {quarter}"), "We have 4");

            state.evilPlayer = 1;
            Assert.AreEqual(state.FormatString("{evil-player} is evil!"), "CEO 2 is evil!");

            state.playerNames[1] = "CO2";
            Assert.AreEqual(state.FormatString("Hello {player-1}!"), "Hello CO2!");
        }

        [Test]
        public void TestRandomTraversal()
        {
            const int numTrials = 1000;
            int numSuccessfulTrials = 0;
            int numFailedTrials = 0;

            int[] finalQuarterStas = new int[GameState.numQuarters + 1];
            List<State> intermediateStates = new List<State>();
            List<State> finalStates = new List<State>();
            HashSet<string> failingChoices = new HashSet<string>();
            HashSet<string> successfulChoices = new HashSet<string>();

            var allLogs = new System.Text.StringBuilder();
            
            var cardCount = new Dictionary<string, int>();

            for (int i = 0; i < numTrials; ++i)
            {
                Debug.LogFormat("Trial {0}", i);
                // TODO(ondrasej): Move the random traversal to a separate class, so that we can do this
                // from multiple tests.
                var gameState = new GameState(cardRepository,
                                              conditionalEffectRepository,
                                              projectRepository,
                                              initialEffects.effects);
                try
                {
                    for (int quarter = 0; quarter < GameState.numQuarters; ++quarter)
                    {
                        Assert.AreEqual(gameState.currentQuarter, quarter);
                        Assert.AreEqual(gameState.companyState.getValue("quarter"), quarter + 1);

                        Assert.AreEqual(gameState.currentQuarterCards.Count, GameState.numPlayers);

                        GameState.NextStep nextStep = GameState.NextStep.NextPlayer;
                        for (int player = 0; player < GameState.numPlayers; ++player)
                        {
                            Assert.AreEqual(gameState.CurrentPlayer, player);
                            var card = gameState.GetCurrentCard();
                            var option = Random.Range(0, card.options.Length);
                            nextStep = gameState.MakePlayerChoice(option);
                        }
                        switch(nextStep)
                        {
                            case GameState.NextStep.EndOfQuarter:
                                gameState.ProcessEndOfQuarter();
                                gameState.MoveToNextQuarter();
                                break;
                            case GameState.NextStep.EndOfGame:
                                break;
                            case GameState.NextStep.NextPlayer:
                                Assert.Fail("We should be at the end of a quarter or at the end of the game");
                                break;
                        }
                        intermediateStates.Add(gameState.companyState.Clone());
                    }
                    ++numSuccessfulTrials;
                    finalQuarterStas[gameState.currentQuarter + 1]++;
                    finalStates.Add(gameState.companyState);
                    successfulChoices.Add(gameState.GetChoiceSummary());
                }
                catch (OutOfCards err)
                {
                    Debug.LogFormat("I'm out of cards! {0}", err);
                    Debug.LogFormat("Log:\n{0}", gameState.GetLog());
                    finalQuarterStas[gameState.currentQuarter]++;
                    ++numFailedTrials;
                    failingChoices.Add(gameState.GetChoiceSummary());
                    allLogs.AppendFormat("Failed at Q{0}\n", gameState.currentQuarter);
                }
                foreach (var quarterCards in gameState.cardLog)
                {
                    foreach (var card in quarterCards)
                    {
                        int count = 0;
                        cardCount.TryGetValue(card.cardName, out count);
                        cardCount[card.cardName] = count + 1;
                    }
                }
                var log = gameState.GetLog();
                Debug.LogFormat("Log:\n{0}", log);
                allLogs.AppendLine(log);
                allLogs.AppendLine();
            }
            Debug.LogFormat("Failed: {0}, Successful: {1}, final quarter stats {2}",
                            numFailedTrials, numSuccessfulTrials,
                            finalQuarterStas.Select(x => x.ToString()).JoinStrings(", "));

            var eur = finalStates.Select(x => x.getValue("eur")).Statistics();
            var eurAll = intermediateStates.Select(x => x.getValue("eur")).Statistics();
            var pr = finalStates.Select(x => x.getValue("pr")).Statistics();
            var prAll = intermediateStates.Select(x => x.getValue("pr")).Statistics();
            var sz = finalStates.Select(x => x.getValue("sz")).Statistics();
            var szAll = intermediateStates.Select(x => x.getValue("sz")).Statistics();
            var pqLovecraft = finalStates.Select(x => x.getValue("pq-lovecraft")).Statistics();
            var pqNoman = finalStates.Select(x => x.getValue("pq-noman")).Statistics();
            Debug.LogFormat("eur: {0} ({1})\n" +
                            "pr: {2} ({3})\n" +
                            "sz: {4} ({5})\n" +
                            "pq-lovecraft: {6},\n" +
                            "pq-noman: {7}\n",
                            eur, eurAll,
                            pr, prAll,
                            sz, szAll,
                            pqLovecraft,
                            pqNoman);

            var cardStats = cardRepository.Cards
                .Select(card => card.cardName)
                .OrderBy(x => x)
                .Select(name => string.Format("{0}: {1}", name, cardCount.GetOrDefault(name, 0)))
                .JoinStrings("\n");
            Debug.Log(cardStats);

            string failingChoicesStr = failingChoices.OrderBy(x => x).JoinStrings("\n");
            Debug.LogFormat("Failing choices: {0}\n{1}", failingChoices.Count, failingChoicesStr);
            using (var writer = new System.IO.StreamWriter("failing-choices.txt"))
            {
                writer.WriteLine(failingChoicesStr);
            }
            using (var writer = new System.IO.StreamWriter("successful-choices.txt"))
            {
                writer.WriteLine(successfulChoices.OrderBy(x => x).JoinStrings("\n"));
            }
            using (var writer = new System.IO.StreamWriter("all-logs.txt"))
            {
                writer.WriteLine(allLogs.ToString());
            }
        }

        [Test]
        public void TestWeightedRandomSort()
        {
            const int numTrials = 11010;
            CardData[] sourceCards = new CardData[] {
                new CardData() { cardName = "Foo", weight = 1000.0f },
                new CardData() { cardName = "Bar", weight = 100.0f },
                new CardData() { cardName = "Zed", weight = 1.0f }
            };

            var counts = sourceCards.Select(x => new int[sourceCards.Length]).ToArray();
            for (int i = 0; i < numTrials; ++i)
            {
                var cards = sourceCards.ToList();
                GameState.WeightedRandomSort(cards);
                Assert.AreEqual(cards.Count, sourceCards.Length);
                for (int j = 0; j < sourceCards.Length; ++j)
                {
                    var card = sourceCards[j];
                    var cardIndex = cards.IndexOf(card);
                    Assert.AreNotEqual(cardIndex, -1);
                    counts[j][cardIndex]++;
                }
            }

            var distribution =
                string.Format("Counts: {0}",
                              counts
                                .Select(x => "{" + x.Select(v => v.ToString())
                                                    .JoinStrings(", ") +
                                             "}")
                                .JoinStrings(", "));
            // These are not guaranteed, but are very high probability. If the test fails,
            // check the distribution in the message, and if it still looks ok, just ignore
            // the failure.
            Assert.GreaterOrEqual(counts[0][0], 8000, distribution);
            Assert.GreaterOrEqual(counts[1][1], 8000, distribution);
            Assert.GreaterOrEqual(counts[2][2], 8000, distribution);

            Assert.GreaterOrEqual(counts[0][1], 800, distribution);
            Assert.LessOrEqual(counts[0][2], 100, distribution);
        }

        [Test]
        public void TestStatNamesAreMatching()
        {
            HashSet<string> writtenStats = new HashSet<string>();
            writtenStats.Add("quarter");  // Updated from the code.
            writtenStats.UnionWith(
                initialEffects.effects.Select(effect => effect.statName));
            writtenStats.UnionWith(
                cardRepository.Cards.SelectMany(card => card.options.SelectMany(
                    option => option.companyEffects.Select(effect => effect.statName))));
            writtenStats.UnionWith(
                conditionalEffectRepository.effects.SelectMany(
                    conditionalEffect => conditionalEffect.effects.Select(
                        effect => effect.statName)));
            writtenStats.UnionWith(
                projectRepository.Projects.SelectMany
                (project => project.initialEffects.Select(effect => effect.statName)));

            HashSet<string> readStats = new HashSet<string>();
            readStats.UnionWith(
                cardRepository.Cards.SelectMany(
                    card => card.requirements.Select(
                        requirement => requirement.statName)));
            readStats.UnionWith(
                conditionalEffectRepository.effects.SelectMany(
                    effect => effect.requirements.Select(
                        requirement => requirement.statName)));
            readStats.UnionWith(
                projectRepository.Projects.SelectMany(
                    project => new string[] { project.qualityStateName,
                                              project.startedStateName }));

            var neverRead = writtenStats
                .Where(stat => !readStats.Contains(stat))
                .OrderBy(x => x)
                .JoinStrings(", ");
            var neverWritten = readStats
                .Where(stat => !writtenStats.Contains(stat))
                .OrderBy(x => x)
                .JoinStrings(", ");
            Debug.LogFormat("Never read: {0}", neverRead);
            Debug.LogFormat("Never written: {0}", neverWritten);

            Assert.IsEmpty(neverRead);
            Assert.IsEmpty(neverWritten);
        }

        [Test]
        public void TestFollowUpCardsAreMatching()
        {
            HashSet<string> cardNames = new HashSet<string>(
                cardRepository.Cards.Select(card => card.cardName));
            foreach (var card in cardRepository.Cards)
            {
                if (string.IsNullOrEmpty(card.samePlayerAsCard)) continue;
                Assert.True(cardNames.Contains(card.samePlayerAsCard),
                            "Unknown card name: {0} in card {1}",
                            card.samePlayerAsCard, card.cardName);
            }
        }
    }
}