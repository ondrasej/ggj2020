﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class StateTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void StateTestsSimplePasses()
        {
            var state = new State();

            state.addEffect("money", 5);
            state.addEffect("money", 5);
            state.addEffect("money", -1);

            Assert.AreEqual(0, state.getValue("china"));
            Assert.AreEqual(9, state.getValue("money"));
        }

        [Test]
        public void ValueValidityTest()
        {

            var state1 = new State();
            state1.addEffect("money", 5);
            state1.addEffect("china", 1);
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.Equal, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 1, RuleType.Greater, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 50, RuleType.Smaller, state1)); 
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.GreaterOrEqual, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.SmallerOrEqual, state1));

        }

        [Test]
        public void EffectsTest()
        {

            var state1 = new State();
            state1.addEffect("money", 5);
            state1.addEffect("china", 1);
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.Equal, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 1, RuleType.Greater, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 50, RuleType.Smaller, state1)); 
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.GreaterOrEqual, state1));
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.SmallerOrEqual, state1));

        }


        [Test]
        public void CardValidityTest()
        {

            var requirement1 = new CardRequirement();
            requirement1.rule = RuleType.Equal;
            requirement1.statName = "china";
            requirement1.value = 1;

            var requirement2 = new CardRequirement();
            requirement2.rule = RuleType.Smaller;
            requirement2.statName = "money";
            requirement2.value = 6;

            var requirement3 = new CardRequirement();
            requirement3.rule = RuleType.Greater;
            requirement3.statName = "money";
            requirement3.value = 4;

            var requirement4 = new CardRequirement();
            requirement4.rule = RuleType.SmallerOrEqual;
            requirement4.statName = "money";
            requirement4.value = 5;

            var requirement5 = new CardRequirement();
            requirement5.rule = RuleType.GreaterOrEqual;
            requirement5.statName = "money";
            requirement5.value = 5;

            var requirement6 = new CardRequirement();
            requirement6.rule = RuleType.SmallerOrEqual;
            requirement6.statName = "china";
            requirement6.value = 10;

            CardData card = ScriptableObject.CreateInstance<CardData>();
            card.requirements = new CardRequirement[] { requirement1, requirement2, requirement3, requirement4, requirement5, requirement6 };

            var state1 = new State();
            state1.addEffect("money", 5);
            state1.addEffect("china", 1);
            Assert.IsTrue(StateUtil.isValueValidByRule("money", 5, RuleType.Equal, state1));
            Assert.IsTrue(StateUtil.isCardValid(card, state1));

            var state2 = new State();
            state2.addEffect("china", 15);
            Assert.IsTrue(!StateUtil.isCardValid(card, state2));

            var state3 = new State();
            state3.addEffect("china", -15);
            StateUtil.MergeTo(state1, state2, state3);
            Assert.IsTrue(StateUtil.isCardValid(card, state1));


        }

    }
}
