﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    public class UtilsTests
    {
        [Test]
        public void TestStatistics()
        {
            var values = new int[] { 3, 4, 5, 6 };
            var stats = values.Statistics();
            Assert.AreEqual(stats.min, 3);
            Assert.AreEqual(stats.max, 6);
            Assert.AreEqual(stats.average, 4.5);

            var valueList = new List<int>(new int[] { 16, 15, 14 });
            stats = valueList.Statistics();
            Assert.AreEqual(stats.max, 16);
            Assert.AreEqual(stats.min, 14);
            Assert.AreEqual(stats.average, 15.0);
        }
    }
}