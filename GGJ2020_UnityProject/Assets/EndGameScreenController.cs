﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScreenController : MonoBehaviour
{
    public Text summaryText = null;

    public GameController gameController = null;

    private void Awake()
    {
        Debug.Assert(summaryText != null);
        Debug.Assert(gameController != null);
    }

    public void UpdateContents()
    {
        var gameState = gameController.gameState;
        if (gameState.currentQuarter >= GameState.numQuarters)
        {
            summaryText.text = gameState.GetFinalText();
        }
        else
        {
            summaryText.text = string.Format(
                "Hrozně mě to mrzí, ale došly nám karty. Herní log:\n{0}",
                gameState.GetLog());
        }
    }
}
