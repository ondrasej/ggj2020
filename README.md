# Video Game Corp.

Have you always wished to change the entire game industry? Because that’s
definitely something that needs a bit of fixing up. In Video Game Corp, you
and two other players take the role of CEO’s of the biggest company in the
world. You and only you three can decide the future of the entire game
industry and maybe even the world. But beware, the stakes are high and
somebody will try to betray you.

This amazing and revolutionary game is a cross between a multiplayer
gamebook, secret identity game, game development simulator, company app and
larp! Local multiplayer for three people! Realistic corporate graphics! Nerve
racking gameplay! Will YOU save the game industry? Or will you be its undoing?

## Credits

A game created (started) during [Global Game Jam Prague
2020](https://globalgamejam.org/2020/jam-sites/global-game-jam-prague).

* Jana Yuffie Kiliánová - design, writing
* Bubuanna Špotová - graphics
* Tomáš Ciboch - code
* Ondra Sýkora - code

## LICENSE

The code and the assets are available under the [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International](LICENSE.md) license.
